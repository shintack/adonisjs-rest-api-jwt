"use strict";

class Contact {
  get rules() {
    return {
      page: "number",
      limit: "number"
    };
  }
  get messages() {
    return {
      "page.number": "Page must be integer.",
      "limit.number": "Limit must be integer."
    };
  }
  async fails(errorMessages) {
    return this.ctx.response.send(errorMessages);
  }
}

module.exports = Contact;
