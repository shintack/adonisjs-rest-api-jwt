"use strict";

const BumblebeeTransformer = use("Bumblebee/Transformer");

/**
 * ContactTransformer class
 *
 * @class ContactTransformer
 * @constructor
 */
class ContactTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  transform(model) {
    return {
      id: model.id,
      name: model.name,
      email: model.email,
      title: model.title,
      tel: model.tel,
      user: model.user
      // add your transformation object here
    };
  }
}

module.exports = ContactTransformer;
