"use strict";

const BumblebeeTransformer = use("Bumblebee/Transformer");

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class UserTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  transform(model) {
    return {
      id: model.id,
      username: model.username,
      email: model.email,
      title: model.title
    };
  }
}

module.exports = UserTransformer;
