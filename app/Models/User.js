"use strict";

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use("Hash");

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");
const PerPage = 10;

class User extends Model {
  static boot() {
    super.boot();

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook("beforeSave", async userInstance => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password);
      }
    });
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany("App/Models/Token");
  }

  contacts() {
    return this.hasMany("App/Models/Contact");
  }

  static userLimitQuery(params) {
    const page = this.validatePaginate(params).page; //params.input("page", 1);
    const limit = this.validatePaginate(params).limit;
    //params.input("perPage",10);
    const keyword = params.input("keyword", null);
    let data;
    if (keyword !== null || keyword !== "") {
      data = this.query()
        .where("username", "like", "%" + keyword + "%")
        .orWhere("email", "like", "%" + keyword + "%")
        .paginate(page, limit);
    } else {
      data = this.query().paginate(page, limit);
    }
    return data;
  }

  static validatePaginate(params) {
    return {
      page:
        params.input("page") !== null && params.input("page") !== ""
          ? params.input("page")
          : 1,
      limit:
        params.input("perPage") !== null && params.input("perPage") !== ""
          ? params.input("perPage")
          : PerPage
    };
  }
}

module.exports = User;
