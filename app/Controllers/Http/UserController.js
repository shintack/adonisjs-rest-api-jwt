"use strict";

const User = use("App/Models/User");
const UserTransformer = use("App/Transformers/UserTransformer");

class UserController {
  async index({ request, transform }) {
    const user = await User.userLimitQuery(request);
    return transform.paginate(user, UserTransformer);
  }
}

module.exports = UserController;
