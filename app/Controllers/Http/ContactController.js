"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Contact = use("App/Models/Contact");
const ContactTransformer = use("App/Transformers/ContactTransformer");

/**
 * Resourceful controller for interacting with contacts
 */
class ContactController {
  /**
   * Show a list of all contacts.
   * GET contacts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, transform, view }) {
    const page = request.input("page", 1);
    const limit = request.input("perPage", 10);
    const keyword = request.input("keyword", null);
    let contacts;
    if (keyword !== null) {
      contacts = await Contact.query()
        .where("name", "like", "%" + keyword + "%")
        .orWhere("email", "like", "%" + keyword + "%")
        .orWhere("title", "like", "%" + keyword + "%")
        .paginate(page, limit);
    } else {
      contacts = await Contact.query().paginate(page, limit);
    }
    return transform.paginate(contacts, ContactTransformer);
  }

  /**
   * Render a form to be used for creating a new contact.
   * GET contacts/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new contact.
   * POST contacts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const name = request.input("name");
    const email = request.input("email");
    const title = request.input("title");
    const tel = request.input("tel");

    const contact = new Contact();
    contact.name = name;
    contact.email = email;
    contact.title = title;
    contact.tel = tel;

    await contact.save();
    return response.json(contact);
  }

  /**
   * Display a single contact.
   * GET contacts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    let contact = await Contact.find(params.id);

    if (!contact) {
      return response.status(404).json({ data: "Resource not found" });
    }

    return response.json(contact);
  }

  /**
   * Render a form to update an existing contact.
   * GET contacts/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update contact details.
   * PUT or PATCH contacts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const name = request.input("name");
    const email = request.input("email");
    const title = request.input("title");
    const tel = request.input("tel");

    let contact = await Contact.find(params.id);

    if (!contact) {
      return response.status(404).json({ data: "Resource not found" });
    }

    contact.name = name;
    contact.email = email;
    contact.title = title;
    contact.tel = tel;
    await contact.save();
    return response.json(contact);
  }

  /**
   * Delete a contact with id.
   * DELETE contacts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const contact = await Contact.find(params.id);
    if (!contact) {
      return response.status(404).json({ data: "Resource not found" });
    }
    await contact.delete();
    return response.json({ message: "Contact deleted!" });
  }
}

module.exports = ContactController;
