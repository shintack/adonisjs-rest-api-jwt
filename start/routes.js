"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.on("/").render("welcome");

Route.post("/auth/register", "AuthController.register");
Route.post("/auth/login", "AuthController.login");

Route.group(() => {
  Route.get("", "ContactController.index").validator("Contact");
  Route.get("/:id", "ContactController.show");
  Route.post("", "ContactController.store").middleware("auth");
  Route.put("/:id", "ContactController.update").middleware("auth");
  Route.delete("/:id", "ContactController.destroy").middleware("auth");
}).prefix("api/contacts");

Route.group(() => {
  Route.get("", "UserController.index");
}).prefix("api/users");
